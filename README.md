# Introductory Nanopore sequencing course

## Aims

This course aims to give you a basic feeling for a *de novo* genome assembly pipeline using only the nanopore data.

We will go through Quality control of data, assembly using Canu, visualize the resulting assembly, we will map the reads that we used during the assembly to the draft genome, attempt a rudimentary annotation and then try to compare whatever we created with a reference sequence.


## Software used <a name="Software"></a>
+ [Nanoplot](https://github.com/wdecoster/NanoPlot)
+ [Canu](https://canu.readthedocs.io/en/latest/quick-start.html)
+ [Bandage](https://rrwick.github.io/Bandage/)
+ [Minimap2](https://lh3.github.io/minimap2/minimap2.html)
+ [SAMtools](http://www.htslib.org/)
+ [prokka](https://github.com/tseemann/prokka)
+ [IGV](https://software.broadinstitute.org/software/igv/)
+ [mummer](https://github.com/mummer4/mummer)


## Data and location <a name="Data"></a>
We will use two datasets in this tutorial.
Main dataset is just a fastq file of nanopore reads that is from the Loman lab [here](http://lab.loman.net/2015/09/24/first-sqk-map-006-experiment/).
Although it is an early dataset from MinION it serves the purpose for a demo very well. It is genomic sequencing of *Escherichia coli* done in 2015.


Additional data at our disposal are the same used in the NanoPolish tutorial [here.](https://nanopolish.readthedocs.io/en/latest/quickstart_consensus.html)

The data itself is a 2kb region that mapped to *Escherichia coli*

Quoted from the tutorial:
> Details:
>  + Sample : E. coli str. K-12 substr. MG1655
>  + Instrument : MinION sequencing R9.4 chemistry
>  + Basecaller : Albacore v2.0.1
>  + Region: “tig00000001:200000-202000”

> You should find the following files:
>  + reads.fasta : subset of basecalled reads
>  + draft.fa : draft genome assembly
>  + draft.fa.fai : draft genome assembly index
>  + fast5_files/ : a directory containing FAST5 files
>  + ecoli_2kb_region.log : a log file for how the dataset was created with nanopolish helper script (scripts/extract_reads_aligned_to_region.py)

Main dataset that we will use for assembly is in `/home/training/Desktop/nanopore/reads/main`

Additional dataset is in `/home/training/Desktop/nanopore/reads/additional`


## A brief introduction to command-line interface
Command-line is a classical way to run programs in (not only) UNIX environment.  
It might not have fancy windows, but it is unsurpassed in terms of power and effective resource cost.  
You need to grasp two concepts to work with command-line:

* paths
* commands  

Path describes a unique location on file-system. It points to a file or a directory.  
You are most likely familiar with windows folder structure. For example `C:\Users\Jonathan\Documents\CakeRecipesJanWillNeverHave\`  

![](images/unix-file-system-i14.gif)

In UNIX systems this is almost the same, just the separator is not `\` but `/`. Unix also does not recognize separate discs in the same way as windows. You will not have `C:\` or `D:\` drives. Instead drives are linked to a folder.  
There are two special "folders" in UNIX environment. They are the "dot" `.` and "dot-dot" `..` folders. A "dot" folder refers to "here", meaning the folder you are in right now. The "dot-dot" folder refers to one folder above the one you are in right now. These exist to simplify the way you write paths, so that you can use a shorter notation and save time.  

The folder where the materials for this course reside is **`/home/training/Desktop/nanopore/`**. In case you want to go to this directory you can use the command line and use a command to change directories.  

This gets us to commands. Commands are programs that preform some function. Commands usually take in some additional information from the user. Such information is called parameter or  an option. These parameters can be, for example, a path to a file, option (switch) denoted by `-` and letter (for example most command recognize `-h` or `--help` to give you quick help), or an option  denoted by `-` and letter that requires additional argument such as path to file, or number etc. (for example `-t [number]` is used by many commands to set number of processors to use).  

To run a command you type in the command name and additional parameters, separated by space. For example to change directory as mentioned you would run a simple command called `cd` and as a parameter you would input the target directory, like this: `cd /home/training/Desktop/nanopore/`.

For further reference feel free to take a look at [this site](http://www.ee.surrey.ac.uk/Teaching/Unix/).

Very brief list of commands you might need:  

Command|Role|Example
-------|------|-------
pwd|prints your current directory|`pwd`
cd|change directory|`cd /home/training/Desktop/nanopore/`
ls|list files in directory|`ls`
cp|copy from to| `cp sourcefile targetfile`
rm|delete file (use with caution, there is no trash-bin)| `rm file`

Few tricky things about terminal:

Shortcut|Effect
-----------------------|----------------------------------------------------
`ctrl+c`| **KILLS** your running process (super effective)
`ctrl+shift+c`|copies from terminal
`ctrl+shift+v`|pastes into terminal
`ctrl+z`|stops/pauses process
`tab`|tab completion (if you are typing, you are doing something wrong)
`up arrow`|list through last commands
`ctrl+r`|start searching command history
`ctrl+l`|clears your terminal window

-------------

## Quality control <a name="QC"></a>

Our bioinformatic journey should begin with looking at what sort of data we have. In some cases, when you have the ONT machine yourself and you run your sequencing, you already have some idea about how the run looked and what sort of output you have. In many other cases you might get the data from the Internet or your friendly sequencing facility. How do you then generate any overview of the data?

### FastQC
One of the most popular sequencing tools that exist for sequencing data is called [FastQC](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/). It works with Fastq data and lets you plot all the information you might need for Illumina and to some extent even nanopore sequencing. For the purpose of this tutorial we will concentrate on other tools, but it might be worth knowing about this one too :)

### NanoPlot <a name="NanoPlot"></a>

For something bit more specific for Nanopore data we can use [Nanoplot](https://github.com/wdecoster/NanoPlot). It is a free and open source tool, that takes in data coming out of ONT sequencers.
It is very flexible in its input and output and you can feed it various type of data like fastq, fasta, bam, fast5 and summary files.
The output will change accordingly.
Let us try it on one of the datasets we have at our disposal. First we will see the plots for the dataset we will use later on for the assembly itself.

``` bash
cd /home/training/Desktop/nanopore
mkdir nanoplot_main
cd nanoplot_main
NanoPlot --fasta ../reads/main/ont_ecoli.fasta
```
To view the report, please run:
``` bash
firefox NanoPlot-report.html
```

We can also see the difference if we allow NanoPlot more information about the run. In this case it is a sequencing summary file from a genomic DNA run performed by our colleagues from EMBL.

``` bash
cd /home/training/Desktop/nanopore
mkdir nanoplot_summary
cd nanoplot_summary
NanoPlot --summary ../reads/additional/sequencing_summary.txt
```

Again to view the report:

``` bash
firefox NanoPlot-report.html
```

------------------------

## Assembly <a name="Assembly"></a>
In case of Illumina sequencing at this point, you would subject your reads to an error correction step.

Thankfully the assembler we will use in this case will do this for you. It is worth mentioning that in some cases people use Canu only for the read correction and then process data elsewhere.

### Canu <a name="Canu"></a>
[Canu](https://canu.readthedocs.io/en/latest/quick-start.html) has became somewhat of a golden standard for nanopore assembly these days. It is a reworked version of Celera assembler - the one that was used to assemble shotgun sequencing in the Human Genome Project.
It does very good job of read correction (you can also use it only for that step in times, when you are not after assembly).

Canu works in few steps:
1. Read correction
2. Read trimming
3. Contig generation

![](images/canu-pipeline.svg)
(Image courtesy of [Koren et al. 2017](https://genome.cshlp.org/content/27/5/722))

Canu allows you to change a plethora of parameters, that control how sensitive the error correction is and subsequent overlap and contig generation is. The downside of this is that it might seem rather intimidating at first and that misuse of the parameters can negatively influence the processing time and memory consumed.

_runtime: ~2 hours_

``` bash
cd /home/training/Desktop/nanopore
mkdir assembly
cd assembly
canu -p ecoli -d . genomeSize=4.8m maxMemory=11G -nanopore-raw ../reads/main/ont_ecoli.fasta
```

What we did so far was to run Canu with default parameters first and then try to tweak after. In this way, you can have some overview of what you can assemble fairly quickly and then tune.

### Assembly Graph Visualization with Bandage <a name="Bandage"></a>

``` bash
Bandage
```

------------------------------------------
## Alignment with Minimap2 <a name="Mapping"></a>



### Minimap2 <a name="Minimap2"></a>

_Runtime: ~2 minutes_

``` bash
module load SAMtools #EMBL specific command to install samtools
cd /home/training/Desktop/nanopore
mkdir alignment
cd alignment 
minimap2 -d ecoli.contigs ../assembly/ecoli.contigs.fasta
minimap2 -ax map-ont ../assembly/ecoli.contigs.fasta ../reads/main/ont_ecoli.fasta | samtools view -b > aln.bam
samtools sort aln.bam -o aln.sorted.bam
samtools index aln.sorted.bam
```



### Mapping Visualization with IGV <a name="IGV"></a>

``` bash
igv.sh
```

------------------------------------------
## Annotation <a name="Annotation"></a>
### Prokka <a name="Prokka"></a>
[Prokka](https://github.com/tseemann/prokka) is a tool for automated annotation for prokaryotic organisms. It downloads various databases upon instalation and then uses those

_Runtime: ~5 minutes_

``` bash
cd /home/training/Desktop/nanopore
mkdir annotation
cd annotation
prokka --outdir . --force --prefix ecoli_prokka ../assembly/ecoli.contigs.fasta
```

### Annotation Visualization <a name="Dnaplotter"></a>
Dnaplotter is a tool that allows for visualization of circular plots of annotation.
We start the program by typing:
``` bash
dnaplotter
```
We will be asked to open a file and in our case we should open the `ecoli_prokka.gff` file. We can play around with the settings and tracks to customize the displayed features.

-----------------------------
## Comparison with a Reference <a name="Ref"></a>
To have some feeling for tool that might be useful in larger scale alignments (think genome to genome), we will play with Mummer. The goal here is to see how well the assembly draft we have aligns to a known *E. coli* reference.

Reference of the *E. coli* genome is in `/home/training/Desktop/nanopore/ref/`


### Mummer <a name="Mummer"></a>
[Mummer](https://github.com/mummer4/mummer) is a very fast aligner capable of genome-wide comparisons.


First we will run the main mummer script:
``` bash
cd /home/training/Desktop/nanopore
mkdir mapping
cd mapping
mummer ../ref/Escherichia_coli_str_k_12_substr_mg1655.ASM584v2.dna.chromosome.Chromosome.fa ../assembly/ecoli.contigs.fasta > mummer.res
```
second, in the same folder we will run the nucmer script to obtain the delta alignment file:
``` bash
nucmer -p nucmer ../ref/Escherichia_coli_str_k_12_substr_mg1655.ASM584v2.dna.chromosome.Chromosome.fa ../assembly/ecoli.contigs.fasta
```


### Mummerplot and show-align <a name="Mummerplot"></a>
Now we can turn the output of mummer into something more visual and easier to read for humans.
``` bash
 mummerplot mummer.res
 show-aligns nucmer.delta Chromosome Tig00000001
```
